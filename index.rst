.. MyFirstDoc documentation master file, created by
   sphinx-quickstart on Sat May  4 22:07:49 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to MyFirstDoc's documentation!
======================================

This is my introduction to my study files.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorial
   project
   code
   markdown

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
