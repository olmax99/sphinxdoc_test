Book ["Mastering Django: Core", George Nigel (2015)]  ...continue Ch.3 Templates

[Django Tutorials](https://docs.djangoproject.com/en/2.2/intro/)

[Django Topics](https://docs.djangoproject.com/en/2.2/topics/)


## DOCKER DJANGO SKEL

###   START NEW DJANGO PROJECT

```
$ docker-compose run web django-admin startproject djangotutorial .
$ cd ../djangowebapp && sudo chown -R $USER:$USER .

```

djangowebapp/djangotutorial/settings.py:
```
    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'postgres',
            'USER': 'postgres',
            'HOST': 'db',
            'PORT': 5432,
        }
    }

```

```
$ docker-compose up -d --build
$ docker-compose exec web sh -c 'python manage.py runserver 0.0.0.0:8000'

```

## DJANGO TUTORIAL

#### STEP 1: Create a new app inside the DjangoProject

[Read more about manage.py (django-admin)](https://docs.djangoproject.com/en/2.2/ref/django-admin/)

In `djangowebapp` directory:
        
`$ python manage.py startapp polls`

django-admin startapp name [directory]
- creates app directory structure in the given app name within the current or target directory

```
djangowebapp
├── djangotutorial
│   ├── __init__.py
│   ├── __pycache__
│   │   ├── __init__.cpython-37.pyc
│   │   ├── settings.cpython-37.pyc
│   │   ├── urls.cpython-37.pyc
│   │   └── wsgi.cpython-37.pyc
│   ├── settings.py
│   ├── urls.py
│   └── wsgi.py
├── Dockerfile
├── manage.py
├── Pipfile
├── Pipfile.lock
└── polls              <-- app created by django-admin
    ├── admin.py
    ├── apps.py
    ├── __init__.py
    ├── migrations
    │   └── __init__.py
    ├── models.py
    ├── tests.py
    └── views.py


```

#### STEP 2: Create a new view

1. In view.py use HttpResponse &sp;&sp;&sp;&sp; &larr; &sp;&sp; html
2. Make new entry in urls.py &sp;&sp;&sp;&sp; &larr; &sp;&sp; **Request App endpoint**
3. include poll.urls in &sp;&sp;&sp;&sp; &larr; &sp;&sp; **Project Table of Contents**  

... continue [https://docs.djangoproject.com/en/2.2/intro/tutorial02/](https://docs.djangoproject.com/en/2.2/intro/tutorial02/)

#### STEP 3: Database Setup

The following message appears at start-up of development server:

*You have 17 unapplied migration(s). Your project may not work properly until you apply*
*the migrations for app(s): admin, auth, cttypes, sessions.*
*Run 'python manage.py migrate' to apply them.*

##### 1. Create initial database tables

```
# list all databases
$ docker-compose exec postgres sh -c 'psql -U husky -l'

# list all tables within a specified db (-c execute command and exit)
$ docker-compose exec postgres sh -c 'psql -U husky -d django_dev -c \\dt'

$ docker-compose exec web sh -c 'python manage.py migrate'

```

##### 2. Include custom models in database

###### STEP 1: Models

Create the respective models in /<custom app>/models.py

The following methods can be applied automatically, once a model is being created inside the db:
```
q = Question(question_text="What's new?", pub_date=timezone.now())
q.save()
q.id
q.question_text
q.pub_date

Question.objects.all()
Question.objects.filter(id=1)
Question.objects.filter(question_text__startswith='What')
Question.objects.get(pub_date__year=current_year)
Question.objects.get(pk=1)   # pk = primary key, same as id

# New objects can also be created on relationships from the API, e.g.
q.<model relationship>_set.create(choice_text='Not much', votes=0)  # here: only choice_set works
q.choice_set.all()
q.choice_set.count()

c = q.choice_set.filter(choice_text__startswith='Just hacking')
c.delete()


```
**NOTE:** What does `_set` mean?
Django's ORM follows the relationship backwards from Question too, automatically generating a field 
on each instance called foo_set where Foo is the model with a ForeignKey field to that model.


###### STEP 2: Register in settings

Add the following line in settings.py:

```
INSTALLED_APPS = [
# CUSTOM APPS
'polls.apps.PollsConfig',   # This is the new app, for which models the tables will be created
    
# DJANGO APPS
'django.contrib.admin',
[ ... ]

```

###### STEP 3: Update database tables

Create a file in /migrations containing the additional migration changes:

```
$ docker-compose exec web sh -c 'python manage.py makemigrations polls'

# OPTIONALLY return the SQL query for a particular migration changes
$ docker-compose exec web sh -c 'python manage.py sqlmigrate polls 0001'

```
NOTE: **Migrations** are how Django stores changes to your models (and thus your database schema).


Create the models of above migration file inside the database:

```
$ docker-compose exec web sh -c 'python manage.py migrate'

# Verify that the migrations have been created in the db tables
$ docker-compose exec postgres sh -c 'psql -U husky -d django_dev -c \\dt'

```
NOTE: The database table `django_migrations` takes track of all changes made !!


#### STEP 4: Admin Site

###### Create user with admin rights:

```
$ docker-compose exec web sh -c 'python manage.py createsuperuser'

```

e.g. admin, admin@example.com, super_secret

Goto `/admin`.

###### Enable model for admin

In polls/admin.py add:

```
from django.contrib import admin

from .models import Question

admin.site.register(Question)


```



---

**HINT:** Use `assert False` at any point of a script in order to see all current run-time parameters
in browser **Django Error Page**



... contine [https://docs.djangoproject.com/en/2.2/intro/tutorial03/](https://docs.djangoproject.com/en/2.2/intro/tutorial03/)


## Mastering Django: Core", George Nigel

#### Templates

The main templating logic consists of 
- variable input
- ifelse
- opererators
- and for loops

##### 1. Variables

´´´
>>> from django.template import Template, Context
>>> t = Template('{{ var }} -- {{ var.upper }} -- {{ var.isdigit }}')  # Only functions without arguments !!
>>> t.render(Context({'var': 'hello'}))
'hello -- HELLO -- False'
>>> t.render(Context({'var': '123'}))
'123 -- 123 -- True'


´´´

Following is the **logical order of templates**:

Template object <html> {{context obj}}</html> <<< Context object {'key1':value, 'key2': value} <<< dict(key, value)

##### 2. Conditions

Three main conditions are allowed: **if, and, or**. They can be nested, but **NOT** chained.

```
{% if athlete_list %}
    {% if coach_list or cheerleader_list %}
        We have athletes, and either coaches or cheerleaders!
    {% endif %}
{% endif %}
{# This is a comment #}


```

Other very useful conditions are **ifequal** and **ifnotequal**:

```
{% ifequal section 'sitenews' %}
    <h1>Site News</h1>
{% else %}
    <h1>No News Here</h1>
{% endifequal %}
{# This is a comment #}


```

##### 3. Loops

Loops can be combined with variables and conditions. On top, they offer a few in-built `forloop` variables:

E.g. `forloop.counter`, `forloop.counter0`, `forloop.first`, `forloop.last`

```
{% comment %}
This is a
multi-line comment.
{% endcomment %}
{% for object in objects %}
    {% if forloop.first %}<li class="first">{% else %}<li>{% endif %}
    {{ object }}
    </li>
{% endfor %}


```

...continue Ch.3 "Using Templates in Views"



