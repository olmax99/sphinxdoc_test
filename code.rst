.. highlight:: python

.. _build-config:

=============
Configuration
=============

.. module:: conf
:synopsis: Build configuration file.

The :term:`configuration directory` must contain a file named :file:`conf.py`.
This file (containing Python code) is called the "build configuration file"
and contains (almost) all configuration needed to customize Sphinx input
and output behavior.

An optional file `docutils.conf`_ can be added to the configuration
directory to adjust `Docutils`_ configuration if not otherwise overridden or
set by Sphinx.

.. _`docutils`: http://docutils.sourceforge.net/
.. _`docutils.conf`: http://docutils.sourceforge.net/docs/user/config.html

The configuration file is executed as Python code at build time (using
:func:`execfile`, and with the current directory set to its containing
directory), and therefore can execute arbitrarily complex code.  Sphinx then
reads simple names from the file's namespace as its configuration.

Important points to note:

* If not otherwise documented, values must be strings, and their default is the
empty string.

* The term "fully-qualified name" refers to a string that names an importable
Python object inside a module; for example, the FQN
``"sphinx.builders.Builder"`` means the ``Builder`` class in the
``sphinx.builders`` module.

* Remember that document names use ``/`` as the path separator and don't
contain the file name extension.

* Since :file:`conf.py` is read as a Python file, the usual rules apply for
encodings and Unicode support.

* The contents of the config namespace are pickled (so that Sphinx can find out
when configuration changes), so it may not contain unpickleable values --
delete them from the namespace with ``del`` if appropriate.  Modules are
removed automatically, so you don't need to ``del`` your imports after use.

.. _conf-tags:

* There is a special object named ``tags`` available in the config file.
It can be used to query and change the tags (see :ref:`tags`).  Use
``tags.has('tag')`` to query, ``tags.add('tag')`` and ``tags.remove('tag')``
to change. Only tags set via the ``-t`` command-line option or via
``tags.add('tag')`` can be queried using ``tags.has('tag')``.
Note that the current builder tag is not available in ``conf.py``, as it is
created *after* the builder is initialized.


Project information
-------------------

.. confval:: project

The documented project's name.

.. confval:: author

The author name(s) of the document.  The default value is ``'unknown'``.

.. confval:: copyright

A copyright statement in the style ``'2008, Author Name'``.

.. confval:: version

The major project version, used as the replacement for ``|version|``.  For
example, for the Python documentation, this may be something like ``2.6``.

.. confval:: release

The full project version, used as the replacement for ``|release|`` and
e.g. in the HTML templates.  For example, for the Python documentation, this
may be something like ``2.6.0rc1``.

If you don't need the separation provided between :confval:`version` and
:confval:`release`, just set them both to the same value.

General configuration
---------------------

.. confval:: extensions

A list of strings that are module names of :doc:`extensions
<extensions/index>`. These can be extensions coming with Sphinx (named
``sphinx.ext.*``) or custom ones.

Note that you can extend :data:`sys.path` within the conf file if your
extensions live in another directory -- but make sure you use absolute paths.
If your extension path is relative to the :term:`configuration directory`,
use :func:`os.path.abspath` like so::

import sys, os

sys.path.append(os.path.abspath('sphinxext'))

extensions = ['extname']

That way, you can load an extension called ``extname`` from the subdirectory
``sphinxext``.

The configuration file itself can be an extension; for that, you only need
to provide a :func:`setup` function in it.

.. confval:: source_suffix

The file extensions of source files.  Sphinx considers the files with this
suffix as sources.  The value can be a dictionary mapping file extensions
to file types.  For example::

source_suffix = {                                                                                                                                                       '.rst': 'restructuredtext',

'.txt': 'restructuredtext',
'.md': 'markdown',
}

By default, Sphinx only supports ``'restructuredtext'`` file type.  You can
add a new file type using source parser extensions.  Please read a document
of the extension to know which file type the extension supports.

The value may also be a list of file extensions: then Sphinx will consider
that they all map to the ``'restructuredtext'`` file type.

Default is ``{'.rst': 'restructuredtext'}``.

.. note:: file extensions have to start with a dot (e.g. ``.rst``).

.. versionchanged:: 1.3
Can now be a list of extensions.

.. versionchanged:: 1.8
Support file type mapping

.. confval:: source_encoding

The encoding of all reST source files.  The recommended encoding, and the
default value, is ``'utf-8-sig'``.

.. versionadded:: 0.5
Previously, Sphinx accepted only UTF-8 encoded sources.

.. confval:: source_parsers

If given, a dictionary of parser classes for different source suffices.  The
keys are the suffix, the values can be either a class or a string giving a
fully-qualified name of a parser class.  The parser class can be either
``docutils.parsers.Parser`` or :class:`sphinx.parsers.Parser`.  Files with a
suffix that is not in the dictionary will be parsed with the default
reStructuredText parser.

